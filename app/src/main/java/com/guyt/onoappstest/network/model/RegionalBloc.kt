package com.guyt.onoappstest.network.model


import com.google.gson.annotations.SerializedName

data class RegionalBloc(
    @SerializedName("acronym")
    val acronym: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("otherAcronyms")
    val otherAcronyms: List<String>? = null,
    @SerializedName("otherNames")
    val otherNames: List<String>? = null
)