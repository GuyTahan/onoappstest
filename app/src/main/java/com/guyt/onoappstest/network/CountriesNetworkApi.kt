package com.guyt.onoappstest.network

import com.guyt.onoappstest.network.model.RestCountriesResponse
import retrofit2.Call
import retrofit2.http.GET

const val BASE_URL = "https://restcountries.eu/rest/v2/"

interface CountriesNetworkAPI{

    @GET("all")
    fun getAllCountries() : Call<RestCountriesResponse>

}