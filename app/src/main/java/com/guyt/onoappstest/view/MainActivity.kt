package com.guyt.onoappstest.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.guyt.onoappstest.viewmodel.OnoViewModel
import com.guyt.onoappstest.R
import com.guyt.onoappstest.logic.CountriesAdapter
import com.guyt.onoappstest.logic.Logic
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Logic.LogicListener {

    private lateinit var logic : Logic

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        countries_list_recycler_view.layoutManager = LinearLayoutManager(this)
        countries_list_recycler_view.addItemDecoration(
            DividerItemDecoration(
                this,
                LinearLayoutManager.VERTICAL
            )
        )

        val viewModel = ViewModelProvider(this).get(OnoViewModel::class.java)
        logic = Logic(this,this, viewModel,this)
        logic.initiateCountryList()

        sort_by_name_button.setOnClickListener{logic.sortCountriesByName()}
        sort_by_area_button.setOnClickListener{logic.sortCountriesBySize()}
    }



    override fun setAdapter(adapter: CountriesAdapter) {
        countries_list_recycler_view.adapter = adapter
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showErrorToast(message: String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast.show()
    }
}
