package com.guyt.onoappstest.logic

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.guyt.onoappstest.viewmodel.OnoViewModel
import com.guyt.onoappstest.R
import com.guyt.onoappstest.network.model.Country
import com.guyt.onoappstest.utils.Resource
import com.guyt.onoappstest.utils.Status
import kotlinx.coroutines.launch

class Logic(
    val lifecycleOwner: LifecycleOwner,
    private val context: Context,
    private val viewModel: OnoViewModel,
    private val UIListener: LogicListener
) {

    interface LogicListener {
        fun setAdapter(adapter: CountriesAdapter)

        fun showProgressBar()

        fun hideProgressBar()

        fun showErrorToast(message: String)
    }

    fun initiateCountryList() {
        viewModel.fetchCountryList().observe(lifecycleOwner, CountryListObserver())
    }

    fun sortCountriesBySize() {
        val countries = viewModel.getCountries()
        lifecycleOwner.lifecycleScope.launch {
            countries?.let { list ->
                UIListener.showProgressBar()
                list.sortByDescending { it.area }
                viewModel.setCountryList(countries)
                UIListener.hideProgressBar()
            }
        }

    }

    fun sortCountriesByName() {
        lifecycleOwner.lifecycleScope.launch {
            val countries = viewModel.getCountries()
            countries?.let { list ->
                UIListener.showProgressBar()
                list.sortBy { it.name }
                viewModel.setCountryList(countries)
                UIListener.hideProgressBar()
            }
        }
    }

    inner class CountryListObserver : Observer<Resource<ArrayList<Country>?>> {
        override fun onChanged(resource: Resource<ArrayList<Country>?>?) {
            resource?.let {
                when (it.status){

                    Status.LOADING -> UIListener.showProgressBar()

                    Status.SUCCESS -> {
                        UIListener.hideProgressBar()
                        if (resource.data != null) {
                            val adapter = CountriesAdapter(context, resource.data)
                            UIListener.setAdapter(adapter)
                        }
                    }

                    Status.ERROR -> {
                        UIListener.hideProgressBar()
                        UIListener.showErrorToast(context.getString(R.string.error_message))
                    }
                }
            }

        }

    }

}


