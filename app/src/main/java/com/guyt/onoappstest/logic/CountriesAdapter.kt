package com.guyt.onoappstest.logic

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.guyt.onoappstest.R
import com.guyt.onoappstest.network.model.Country
import kotlinx.android.synthetic.main.country_view_holder.view.*

class CountriesAdapter(private val context: Context, private var countries: ArrayList<Country>) :
    RecyclerView.Adapter<CountriesAdapter.CountryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.country_view_holder,
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        val country = countries[position]
        holder.setCountry(country)
    }

    class CountryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private var country : Country? = null


        fun setCountry(country: Country) {
            this.country = country
            this.itemView.name.text = country.name
            this.itemView.nativeName.text = country.nativeName
        }
    }

}