package com.guyt.onoappstest.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}