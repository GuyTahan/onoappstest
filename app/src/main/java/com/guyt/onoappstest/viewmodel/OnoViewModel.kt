package com.guyt.onoappstest.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.guyt.onoappstest.network.BASE_URL
import com.guyt.onoappstest.network.CountriesNetworkAPI
import com.guyt.onoappstest.network.model.Country
import com.guyt.onoappstest.network.model.RestCountriesResponse
import com.guyt.onoappstest.utils.Resource
import com.guyt.onoappstest.utils.Status
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OnoViewModel(application: Application) : AndroidViewModel(application) {

    private var countries: MutableLiveData<Resource<ArrayList<Country>?>> = MutableLiveData(Resource.loading(null))

    fun fetchCountryList() : MutableLiveData<Resource<ArrayList<Country>?>> {
        if (countries.value?.status != Status.SUCCESS){
            fetchCountriesFromNetwork()
        }
        return countries
    }


    private fun fetchCountriesFromNetwork() {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        val service = retrofit.create(CountriesNetworkAPI::class.java)

        service.getAllCountries().enqueue(object : Callback<RestCountriesResponse> {

            override fun onFailure(call: Call<RestCountriesResponse>, t: Throwable) {
                countries.postValue(Resource.error(t.localizedMessage, null))
            }

            override fun onResponse(call: Call<RestCountriesResponse>, response: Response<RestCountriesResponse>) {
                countries.postValue(Resource.success(response.body()))
            }

        })
    }

    fun getCountries() : ArrayList<Country>? {
        return countries.value?.data
    }

    fun setCountryList(countryList: java.util.ArrayList<Country>) {
        countries.postValue(Resource.success(countryList))
    }

}